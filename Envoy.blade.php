@setup
    $dir = "/var/www/project";
    $servers = ['main' => $main_ip, 'second' => $second_ip];
    $hosts = ['main', 'second'];
@endsetup

@servers($servers)

@story('deploy', ['on' => $hosts, 'parallel' => true])
    git
    composer
@endstory

@task('git')
    cd {{ $dir }}
    git checkout .
    git checkout {{ $branch }}
    git pull origin {{ $branch }}
@endtask

@task('composer')
    cd {{ $dir }}
    composer install
@endtask

@finished
    @slack('https://hooks.slack.com/services/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', '#deploy', $finish)
@endfinished
