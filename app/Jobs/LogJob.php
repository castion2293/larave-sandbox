<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $dataTime = '';

    /**
     * Create a new job instance.
     *
     * @param string $dataTime
     */
    public function __construct(string $dataTime)
    {
        $this->dataTime = $dataTime;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info('使用 Redis Queue 寫紀錄時間: ' . $this->dataTime);
    }
}
