<?php

namespace App\Console\Commands;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Console\Command;
use RuntimeException;

class BugsnagCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bugsnag:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'test Bugsnag';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Bugsnag::notifyException(new RuntimeException("Test error"));

        return 0;
    }
}
